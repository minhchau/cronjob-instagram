<?php
	function num_format($num, $precision = 1) {
	   if ($num >= 1000 && $num < 1000000) {
			$n_format = number_format($num/1000,$precision = 0).'K';
		} else if ($num >= 1000000 && $num < 1000000000) {
		$n_format = number_format($num/1000000,$precision = 1).'M';
	   } else if ($num >= 1000000000) {
	   $n_format=number_format($num/1000000000,$precision).'B';
	   } else {
	   $n_format = $num;
		}
	  return $n_format;
  } 
?>
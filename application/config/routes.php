<?php defined('BASEPATH') OR exit('No direct script access allowed');
$route['404_override']         = '';
$route['translate_uri_dashes'] = TRUE;
$route['default_controller']   = 'views/dashboard';

$route['dashboard']		= 'follow/index';
$route['execute/(:num)'] = 'execute/index/$1';
$route['follow/(:num)'] = 'follow/index/$1';
$route['like/(:num)'] = 'like/index/$1';
$route['test/(:num)'] = 'test/index/$1';
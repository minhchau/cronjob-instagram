<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


$config['mongo_db']['active'] = 'default';

$config['mongo_db']['default']['no_auth'] = true;
$config['mongo_db']['default']['port'] = '27017';
$config['mongo_db']['default']['username'] = 'username';
$config['mongo_db']['default']['password'] = 'password';
$config['mongo_db']['default']['db_debug'] = TRUE;
$config['mongo_db']['default']['return_as'] = 'array';
$config['mongo_db']['default']['write_concerns'] = 1;
$config['mongo_db']['default']['journal'] = true;
$config['mongo_db']['default']['read_preference'] = NULL;
$config['mongo_db']['default']['read_preference_tags'] = NULL;

$config['mongo_db']['default']['hostname']   = '127.0.0.1';
//$config['mongo_db']['default']['hostname'] = '108.61.194.147';

//$config['mongo_db']['default']['database']  = 'project';
//$config['mongo_db']['default']['database']  = 'spyultimate';
$config['mongo_db']['default']['database']     = 'pacman';

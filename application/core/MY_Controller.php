<?php (defined('BASEPATH')) OR exit('No direct script access allowed');
class MY_Controller extends CI_Controller{
	public $template  =  'template/main';
	public $user_id;
	public $user_email;
	public $user_level;
	public $user_token;
	public $data = array();
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('layout_helper','string','cookie'));
		//$this->check_session();
		$this->user_id = 45;
 	}
	
	public function view($view)
	{
		$this->data['view'] = $view;
		$this->load->view($this->template,$this->data);	
	}
	
	public function check_session()
	{
		if(!isset($_SESSION['user_id']) || !isset($_SESSION['user_email']) || !isset($_SESSION['token'])){
			$this->unauthorized();
		}
		$this->user_id    = $_SESSION['user_id'];
		$this->user_email = $_SESSION['user_email'];
		$this->user_token = $_SESSION['token'];

	}
	
	public function unauthorized()
	{
		header('HTTP/1.1 401 Unauthorized', true, 401);
		$veriry_url = urlencode(base_url().'authentication');
		echo'<script>';
			echo'location.href="https://auth.fptultimate.com?redirect='.$veriry_url.'"';
		echo'</script>';
		die;		
	}
	
	

	
}
?>
<?php  (defined('BASEPATH')) OR exit('No direct script access allowed');
class Proxy extends MY_Controller
{	
	public function __construct(){
   		parent::__construct();
		$this->load->library('curl');
		$this->load->helper('response');
		
   	}
	public function add_proxy(){
		$name 		= $this ->input->post('name');
		$proxy	    = $this->input->post('proxy');
		$proxy      = str_replace(' ','',$proxy);
		
		if(!$name || !$proxy)
		{
			echo response(400,'Error');	
			exit;
		}
		
		
		$check_proxy = $this->test_proxy($proxy);
		if($check_proxy)
		{
			$newInsert = array(
			'user_id' 	=>$this->user_id,
			'name'		=>$name,
			'proxy'		=>$proxy,
			'created'	=>time(),
			'status'	=> 1
			);
			$this->db->insert('proxys',$newInsert);
			echo response(200,'Insert Proxy Success');	
		}else{
			echo response(400,'Proxy khong ton tai');
			exit;
		}
		
	}
	public function edit_proxy(){
		$id 		= $this->input->post('id');
		$name 		= $this ->input->post('name');
		$proxy	    = $this->input->post('proxy');
		$proxy      = str_replace(' ','',$proxy);
		
		if(!$name || !$proxy)
		{
			echo response(400,'Error');	
			exit;
		}
		
		$check_proxy = $this->test_proxy($proxy);
		
		if($check_proxy)
		{
			$this->db->where(array('user_id'=>$this->user_id,'id'=>$id))
			->set(array('name'=>$name,'proxy'=>$proxy))
			->update('proxys');
			echo response(200,'Success');	
			
		}else{
			echo response(400,'Proxy khong ton tai');
			exit;
		}
		
	}
	public function remove_proxy(){
		$id = $this->input->post('id');
		$this->db->where(array('user_id' =>$this->user_id,'id'=>$id))->delete('proxys');
		echo response(200,'Remove Success');	
	}
	
	private function test_proxy($proxy){
		
		$url = 'https://api.ipify.org/';
		$res = $this->curl->call($url,false,false,false,'',$proxy,false);
		if($res['code'] == 200 && $res['response']) {
			return true;	
		}
		else
			return false;
		
		
	}
	
	private function getRealIpAddr()
	{
		if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
		{
		  $ip=$_SERVER['HTTP_CLIENT_IP'];
		}
		elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
		{
		  $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
		}
		else
		{
		  $ip=$_SERVER['REMOTE_ADDR'];
		}
		return $ip;
	}
}?>
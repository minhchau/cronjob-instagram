<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class Follow extends CI_Controller
{
    public $acc_id;
    public function __construct()
    {
        parent::__construct();
        $this->load->library('curl');
        date_default_timezone_set("America/Los_Angeles");
    }

    public function index($id)
    {
        $max_time_exec = time() + 100;//Tối đa 110 giây

        $this->acc_id = $id;

        $param = array(
            'acc_id'    => $this->acc_id,
            'proxy'     => true,
            'action_id' => 2,
        );

        $res = $this->curl->instadaily('account/get', true, $param);

        $res = json_decode($res, true);

        if (empty($res['data'])) {
            exit();
        }


        $acc     = current($res['data']);
        $cookies = json_decode($acc['cookies'], true);
        $proxy   = $acc['proxy'];

        // lấy dữ liệu cài đặt (table setting)
        $setting = $this->db->where('acc_id',$this->acc_id)->get('settings')->row_array();

        // lấy dữ liệu nguồn follow (source users)
        $source  = $this->get_source($setting['id'],$cookies,$proxy);

        if($source == false) {
            $update_action = array(
                'error' => 1
            );
            $update_action = json_encode($update_action);
            $this->curl->instadaily('action/update', true, array('id'=>$acc['aca_id'],'data'=>$update_action));
            exit;
        }

        sleep(1);

        $update_action = array(
            'results' => $acc['results']
        );
        $update_setting = array(
            'max_day' => $setting['max_day'],
            'max_ope' => $setting['max_ope']
        );

        $this_total = count($source['users']);

        foreach($source['users'] as $row)
        {

            if(time() >= $max_time_exec) {//Nếu vượt quá thời gian thực thi script là 110 giây thì thoát
                break;
            }

            $row = current($row);

            $this_total--;

            if($row['followed_by_viewer'] == 1 || $row['requested_by_viewer'] == 1) {
                continue;
            }

            if($setting['has_profile_img'] == 1 && $this->check_avatar($row['profile_pic_url']) === false) {
                continue;
            }

            if ($setting['private_user'] == 1 && $this->check_private($row['id']) == true) {
                continue;
            }

            $exec = $this->exec_follow($row['id'], $cookies, $proxy);


            if($exec !== false)
            {
                //Cập nhập lại cookie mới
                foreach($exec['cookies'] as $key => $cookie) {
                    $cookies[$key] = $cookie;
                }

                $update_action['results']++;
                $update_setting['max_day']--;
                $update_setting['max_ope']--;


                if($update_setting['max_day'] <= 0) {//Đạt giới hạn follow trong ngày

                    $follow_day = json_decode($setting['follow_day'],true);
                    $follow_ope = json_decode($setting['follow_ope'],true);
                    $update_setting['max_ope'] = rand($follow_ope[0],$follow_ope[1]);
                    $update_setting['max_day'] = rand($follow_day[0],$follow_day[1]);

                    $update_action['time_run'] = $this->get_time($setting['delay'], $setting['execute_time'],2);

                    break;
                }
                if($update_setting['max_ope'] <= 0) {//Đạt giới hạn follow trong 1 lượt

                    $follow_ope = json_decode($setting['follow_ope'],true);
                    $update_setting['max_ope'] = rand($follow_ope[0],$follow_ope[1]);

                    $update_action['time_run'] = $this->get_time($setting['delay'],$setting['execute_time'],1);

                    break;
                }

            }
            else {
                $update_action['error'] = 1;
                break;
            }

            $sleep = json_decode($setting['sleep'],true);
            $sleep = rand($sleep[0],$sleep[1]);
            sleep($sleep);

        }

        //Nếu trả lời hết 12 người thì mới update next_page cho bảng source_users

        if($this_total == 0) {
            $update_source = array(
                'next_page' => $source['next_page']
            );
            if($source['end']) {
                $update_source['status'] = 2;
            }

            $this->db->where('id',$source['id'])->set($update_source)->update('source_users');
        }
        //Update bảng setting
        $this->db->where('id',$setting['id'])->set($update_setting)->update('settings');

        //Update account
        $update_account = array(
            'cookies' => json_encode($cookies),
            'token'   => $cookies['csrftoken']
        );

        $this->curl->instadaily('account/update',true,array('id'=>$this->acc_id, 'data'=>json_encode($update_account)));
        //update account action
        $this->curl->instadaily('action/update',true,array('id'=>$acc['aca_id'], 'data'=>json_encode($update_action)));

    }




    private function get_source($setting_id,$cookies,$proxy)
    {
        $source = $this->db->where(array('setting_id'=>$setting_id,'status'=>1))->order_by('id','asc')->limit(1)->get('source_users')->row_array();

        $insta_id = $source['pk'];

        if ( !$source['next_page']) {
            $param = array(
                'query_hash' => '37479f2b8209594dde7facb0d904896a',
                'variables' => '{"id":"' . $insta_id . '","first":50,"after":""}'
            );
        } else {
            $param = array(
                'query_hash' => '37479f2b8209594dde7facb0d904896a',
                'variables' => '{"id":"' . $insta_id . '","first":50,"after":"' . $source['next_page'] . '"}'
            );
        }

        $header = array(
            'x-csrftoken: ' . $cookies['csrftoken'],
            'cookie: ' . $this->cookie_string($cookies),
        );

        $param = http_build_query($param);

        $res = $this->curl->instagram('graphql/query/?'.$param, false, false, $header,$proxy);

        $response = json_decode($res['response'],true);

        if($res['code'] != 200 || json_last_error() !== JSON_ERROR_NONE || !isset($response['status']) || $response['status'] != 'ok') {
            $log = array(
                'acc_id' => $this->acc_id,
                'type' => 'source',
                'code'   => $res['code'],
                'header' => '',
                'response' => $res['response'],
                'created' => time()
            );
            $this->db->insert('logs',$log);
            return false;
        }

        $users     =  $response['data']['user']['edge_followed_by']['edges'];
        $next_page =  $response['data']['user']['edge_followed_by']['page_info']['end_cursor'];
        $has_page  =  $response['data']['user']['edge_followed_by']['page_info']['has_next_page'];

        return array(
            'id'        => $source['id'],
            'users'     => $users,
            'next_page' => $next_page,
            'end'       => $has_page ? 0 : 1,
        );

    }

    private function exec_follow($id, $cookies, $proxy)
    {
        $url = 'web/friendships/'.$id.'/follow/';

        $header = array(
            'content-type: application/x-www-form-urlencoded',
            'x-csrftoken: ' . $cookies['csrftoken'],
            'x-instagram-ajax: cba3c1a41010',
            'cookie: ' . $this->cookie_string($cookies),
            'x-requested-with: XMLHttpRequest'
        );

        $res = $this->curl->instagram($url,true,array('a'=>'b'),$header,$proxy,true);


        $response = json_decode($res['response'],true);

        //Nếu bị lỗi thì return lỗi và kết quả báo lỗi
        if($res['code'] != 200 || json_last_error() !== JSON_ERROR_NONE || !isset($response['status']) || $response['status'] != 'ok') {
            $log = array(
                'acc_id'   => $this->acc_id,
                'type'     => 'exec',
                'code'     => $res['code'],
                'header'   => $res['header'],
                'response' => $res['response'],
                'created'  => time()
            );

            $this->db->insert('logs',$log);

            return false;
        }
        //không lỗi thì trả về success true
        return array(
            'success' => true,
            'cookies' => $this->get_cookie($res['header'])
        );

    }

    private function check_avatar($url)
    {
        if(preg_match('/\/5BBBE67A\//',$url))
            return false;
        else
            return true;
    }


    private function check_private($id) {
        $user = $this->getInfo($id);

        if($user && $user['is_private'] == true) {
            return true;
        }

        return false;
    }


    private function get_time($delay,$exec_time,$type)
    {

        $exec_time = json_decode($exec_time,true);

        $ext_1 = strtotime('TODAY') + $exec_time[0] * 3600;

        $ext_2 = strtotime('TODAY') + $exec_time[1] * 3600;

        if($type == 1) {

            $delay  = json_decode($delay,true);
            $delay  = rand($delay[0],$delay[1]) * 60; //Giây

            $nexttime = $delay + time();

            if($ext_2 < $ext_1) {
                $ext_2 += 86400;
            }

            if($nexttime >= $ext_2) {
                $nexttime = $ext_1 + 86400;
            }

            return $nexttime;
        }

        else {
            $nexttime = $ext_1 + 86400;
            if($ext_2 < $ext_1) {
                $nexttime = strtotime('TODAY') + 86400;
            }
            return $nexttime;
        }

    }

    // thông tin user instagram
    private function getInfo($insta_id)
    {
        $url = 'https://i.instagram.com/api/v1/users/' . $insta_id . '/info/';
        $res = $this->curl->call($url);
        $res = json_decode($res['response'], true);

        if(isset($res['status']) && $res['status'] == 'ok'){
            return $res['user'];
        }

        return array();
    }

    private function get_cookie($header)
    {
        preg_match_all('/^Set-Cookie:\s*([^;]*)/mi', $header, $matches);

        $cookies = array();

        foreach ($matches[1] as $item) {
            parse_str($item, $cookie);
            if (!trim(str_replace('"', '', current($cookie)))) {
                continue;
            }
            $cookies = array_merge($cookies, $cookie);
        }

        return $cookies;
    }

    private function cookie_string($cookies)
    {
        $cookie_string = '';
        foreach ($cookies as $key => $cookie) {

            $cookie_string .= $key . '=' . $cookie . ';';
        }
        return $cookie_string;
    }
}
?>
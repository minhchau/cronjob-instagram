<?php (defined('BASEPATH')) OR exit('No direct script access allowed');
class Execute extends CI_Controller
{
    public $acc_id;
	
	public function __construct()
    {
        parent::__construct();
        $this->load->library('curl');
		date_default_timezone_set("America/Los_Angeles");
    }
	
	public function index($id)
    {
        $max_time_exec = time() + 150;//Tối đa 150 giây
		
		if(!isset($id)){
			exit;	
		}
		
		$acc = $this->input->post();		
		
		if(!$acc) {
			exit;	
		}
		
		$this->acc_id = $id;		
		
		$cookies = json_decode($acc['cookies'],true);
		$setup = json_decode($acc['setting'],true);
		$proxy   = $acc['proxy'];
		$proxy_auth = $acc['proxy_auth'];
		$aca_id  = $acc['aca_id'];

        $setting = $this->db->select('max_day,max_ope,result_day,result_ope')->where('acc_id',$this->acc_id)->get('settings')->row_array();
		
        $source  = $this->get_source($cookies, $proxy, $proxy_auth);
		
		if($source == false) {
			$update_action = array(
				'rate_limit' => 1,
                'time_run' => time() + 86400
			);
			$this->curl->instadaily('action/update/'.$acc['aca_id'], true, $update_action);
			exit;	
		}
		
		sleep(1);
		
		$update_action = array(
			'results' => $acc['results']
		);
		
		$this_total = count($source['users']);
		
		foreach($source['users'] as $row) 
		{
			
			if(time() >= $max_time_exec) {//Nếu vượt quá thời gian thực thi script là 110 giây thì thoát
				$put = $max_time_exec.'------------------'.time();
				file_put_contents('test.txt',$put);
				break;	
			}
			
			$row = current($row);
			
			$this_total--;
			
			if($row['followed_by_viewer'] == 1 || $row['requested_by_viewer'] == 1) {
				continue;	
			}
			
			if($setup['has_profile_img'] == 1 && $this->check_avatar($row['profile_pic_url']) === false) {
				continue;
			}
			
			if ($setup['private_user'] == 1 && $this->check_private($row['id']) == true) {
				continue;
			}
			
            if ($this->db->from('followings')->where('insta_id', $row['id'])->count_all_results() > 0 ){
                continue;
            }
			
			$exec = $this->exec_follow($row['id'], $cookies, $proxy, $proxy_auth);
			if ($exec == 4) {
			    sleep(5);
			    continue;
            }
			
			
			if($exec !== false) 
			{
				//Cập nhập lại cookie mới
				foreach($exec['cookies'] as $key => $cookie) {
					$cookies[$key] = $cookie;
				}
				
				$followings[] = array(
					'insta_id'=>$row['id'],
					'username' => $row['username'],
					'acc_id' => $this->acc_id,
					'created' => time()
				);
							
				$update_action['results']++;				
				
				$setting['result_day']++;
				$setting['result_ope']++;
				
				
				if($setting['max_day'] <= $setting['result_day']) {//Đạt giới hạn follow trong ngày
					
					$setting['max_ope'] = rand($setup['follow_ope'][0],$setup['follow_ope'][1]);
					$setting['max_day'] = rand($setup['follow_day'][0],$setup['follow_day'][1]);
					$setting['result_day'] = 0;
					$setting['result_ope'] = 0;
										
					$update_action['time_run'] = $this->get_time($setup['delay_ope'],$setup['execute_time'],2);
										
					break;	
				}
				
				if($setting['max_ope'] <= $setting['result_ope']) {//Đạt giới hạn follow trong 1 lượt
					
					$setting['max_ope']    = rand($setup['follow_ope'][0],$setup['follow_ope'][1]);
					$setting['result_ope'] = 0;
					
					$get_time = $this->get_time($setup['delay_ope'],$setup['execute_time'],1);
					
					if($get_time['new_day'] == true) {
						$setting['max_day'] = rand($setup['follow_day'][0],$setup['follow_day'][1]);
						$setting['result_day'] = 0;
					}
					
					$update_action['time_run'] = $get_time['nexttime'];
                    $update_action['rate_limit'] = 0;

					
					break;	
				}
			
			} 
			else {
				$update_action['rate_limit'] = 1;
                $update_action['time_run'] = time() + 86400;
				break;
			}
			
			$sleep = $setup['delay_follow'];
			$sleep = rand($sleep[0],$sleep[1]);
			sleep($sleep);
			
		}
		
		//Nếu trả lời hết 12 người thì mới update next_page cho bảng source_users
		
		if($this_total == 0) 
		{
			$update_source['next_page'] = $source['next_page'];
			if($source['end']) {
				$update_source['status'] = 2;	
			}
			$this->db->where('id',$source['id'])->set($update_source)->update('source_users');	
		}
		
		if(isset($followings)) {
			$this->db->insert_batch('followings',$followings);	
		}
		
		$this->db->where('acc_id',$this->acc_id)->set($setting)->update('settings');
		
		//Update account	
		$update_account = array(
			'cookies' => json_encode($cookies)
		);
		
	   $this->curl->instadaily('account/update/'.$this->acc_id, true, $update_account);
	   //update account action
	   $this->curl->instadaily('action/update/'.$acc['aca_id'], true, $update_action);

    } 

	
	private function get_source($cookies,$proxy, $proxy_auth)
    {
        $source = $this->db->where(array('acc_id'=>$this->acc_id,'status'=>1))->order_by('id','asc')->limit(1)->get('source_users')->row_array();
		
        $insta_id = $source['pk'];

        if ( !$source['next_page']) {
            $param = array(
                'query_hash' => '37479f2b8209594dde7facb0d904896a',
                'variables' => '{"id":"' . $insta_id . '","first":50,"after":""}'
            );
        } else {
            $param = array(
                'query_hash' => '37479f2b8209594dde7facb0d904896a',
                'variables' => '{"id":"' . $insta_id . '","first":50,"after":"' . $source['next_page'] . '"}'
            );
        }
		
        $header = array(
			'x-csrftoken: ' . $cookies['csrftoken'],
            'cookie: ' . $this->cookie_string($cookies),
        );
		
		$param = http_build_query($param);
		
        $res = $this->curl->instagram('graphql/query/?'.$param, false, false, $header,$proxy, '', $proxy_auth);
		
		$response = json_decode($res['response'],true);

		if ($res['code'] == 0) {
		    die;
        }
		
		if($res['code'] != 200 || json_last_error() !== JSON_ERROR_NONE || !isset($response['status']) || $response['status'] != 'ok') {
			$log = array(
				'acc_id' => $this->acc_id,
				'type' => 'source',
				'code'   => $res['code'],
				'header' => '',
				'response' => $res['response'],
				'created' => time()
			);
			$this->db->insert('logs',$log);
			return false;
		}
		
		$users     =  $response['data']['user']['edge_followed_by']['edges'];
		$next_page =  $response['data']['user']['edge_followed_by']['page_info']['end_cursor'];
		$has_page  =  $response['data']['user']['edge_followed_by']['page_info']['has_next_page'];

        return array(
			'id'        => $source['id'],
			'users'     => $users,
			'next_page' => $next_page,
			'end'       => $has_page ? 0 : 1,
		);
		
    }
	
	private function exec_follow($id, $cookies, $proxy, $proxy_auth)
    {
        $url = 'web/friendships/'.$id.'/follow/';
		
        $header = array(
			'content-type: application/x-www-form-urlencoded',
            'x-csrftoken: ' . $cookies['csrftoken'],
            'x-instagram-ajax: cba3c1a41010',
            'cookie: ' . $this->cookie_string($cookies),
            'x-requested-with: XMLHttpRequest'
        );

		$res = $this->curl->instagram($url,true,array('a'=>'b'),$header,$proxy,true, $proxy_auth);
		
		
		$response = json_decode($res['response'],true);

		if ($res['code'] == 0) {
		    return 4;
        }
		//Nếu bị lỗi thì return lỗi và kết quả báo lỗi
		if($res['code'] != 200 || json_last_error() !== JSON_ERROR_NONE || !isset($response['status']) || $response['status'] != 'ok') {
			$log = array(
				'acc_id'   => $this->acc_id,
				'type'     => 'exec',
				'code'     => $res['code'],
				'header'   => $res['header'],
				'response' => $res['response'],
				'created'  => time()
			);
			
			$this->db->insert('logs',$log);
				
			return false;
		}
		//không lỗi thì trả về success true
		return array(
			'success' => true,
			'cookies' => $this->get_cookie($res['header'])
		);	
		
    }  
	
	private function check_avatar($url)
	{
		if(preg_match('/\/5BBBE67A\//',$url)) 
			return false;
		else
			return true;
	}

	
	private function check_private($id)
	{
		$user = $this->getInfo($id);
		
		if($user && $user['is_private'] == true) {
			return true;	
		}
		
		return false;
	}

    
	private function get_time($delay,$exec_time,$type)
	{
		$timetoday = strtotime('TODAY');

		$start = $timetoday + $exec_time[0] * 3600;
			
		$end   = $timetoday + $exec_time[1] * 3600; 
		
		if($end < $start) 
		{
			if(intval(date('H')) < $exec_time[0]) {
				$start -= 86400;	
			} 
			else {
				$end += 86400;	
			}
			
		}
		
		if($type == 1) {
			
			$delay  = rand($delay[0],$delay[1]) * 60;
			
			$nexttime = $delay + time();			
			
			$new_day = false;
						
			if($nexttime >= $end) {
				$nexttime = $start + 86400;	
				$new_day = true;
			}
			
			return array('nexttime'=>$nexttime,'new_day'=>$new_day);
		}
		
		else {
			$nexttime = $start + 86400;	
			return $nexttime;
		}
		
	}

    // thông tin user instagram
    private function getInfo($insta_id)
    {
        $url = 'https://i.instagram.com/api/v1/users/' . $insta_id . '/info/';
        $res = $this->curl->call($url);
        $res = json_decode($res['response'], true);
		
		if(isset($res['status']) && $res['status'] == 'ok'){
			return $res['user'];	
		}
		
		return array();
    }
	
	private function get_cookie($header)
    {
        preg_match_all('/^Set-Cookie:\s*([^;]*)/mi', $header, $matches);

        $cookies = array();

        foreach ($matches[1] as $item) {
            parse_str($item, $cookie);
            if (!trim(str_replace('"', '', current($cookie)))) {
                continue;
            }
            $cookies = array_merge($cookies, $cookie);
        }
	
        return $cookies;
    }

    private function cookie_string($cookies)
    {
        $cookie_string = '';
        foreach ($cookies as $key => $cookie) {

            $cookie_string .= $key . '=' . $cookie . ';';
        }
        return $cookie_string;
    }
}
?>
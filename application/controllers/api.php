<?php (defined('BASEPATH')) OR exit('No direct script access allowed');
class Api extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('curl');
    }

    public function get($acc_id)
    {
        $setting = $this->db->where('acc_id', $acc_id)->get('settings')->row_array();
        $return = array(
            'setting' => array(),
            'source' => array()

        );
        if (!empty($setting)) 
		{
            $followSource = $this->db->where('setting_id', $setting['id'])->get('source_users')->result_array();
			
            $return = array(
                'setting' => $setting,
                'source'  => $followSource,
            );

        }
        echo response(200, $return);

    }

    /*
     *  get all log ($acc_id)
     */
    public function getLog($acc_id)
    {
        if(!$acc_id) {
            exit();
        }

        $log = $this->db->where('acc_id', $acc_id)->get('logs')->result_array();
        $return = array(
            'log' => $log,
        );
        echo response(200, $return);

    }

    /*
     *  get all log ($acc_id)
     */
    public function addLog($acc_id)
    {
        if(!$acc_id) {
            exit();
        }

        $source  = $this->input->post('source');
        $this->db->insert('logs', $source);
        return true;

    }

    public function add($acc_id)
    {
        if(!isset($acc_id)) {
			exit;	
		}
		
		$check = $this->db->where('acc_id', $acc_id)->get('settings')->num_rows();
		
		if($check > 0) {
			exit;
		}
		
		$setting = $this->input->post('setting');
		$source  = $this->input->post('source');
		
		if(!$setting || !$source){
			 echo response(400, 'Fields invalid');
            exit;
		}
		
		$setting  = json_decode($setting,true);
		$source   = json_decode($source,true);
		
		
		$this->db->insert('settings', $setting);
	
		$this->db->insert_batch('source_users', $source['users']);
		
		echo response(200, 'insert');
    }
	
	public function update($acc_id)
	{
		if(!isset($acc_id))
		{
			exit;	
		}
		
		$setting = $this->input->post('setting');
		$source  = $this->input->post('source');
		
		if(!$setting || !$source){
			 echo response(400, 'Fields invalid');
             exit;
		}
		
		$setting = json_decode($setting,true);
		$source  = json_decode($source,true);
	
		$this->db->where('acc_id',$acc_id)->set($setting)->update('settings');
		
		$users = $source['users'];
		
		$source_old = $this->db->where('acc_id',$acc_id)->get('source_users')->result_array();
		
		$source_old_id = array_column($source_old,'pk');
		
		foreach($users as $key => $row) 
		{
			if (($old_key = array_search($row['pk'], $source_old_id)) !== false) {
				
				if($source_old[$old_key]['status'] != 4) {
					unset($source_old[$old_key]); 	
				}
				
				unset($users[$key]);
			}
			else {
				$users[$key]['acc_id'] = $acc_id;
			}
		}
		
		if($users) {
			$this->db->insert_batch('source_users', $users);
		}
		
		if($source_old) {
			
			foreach($source_old as $key => $row) {
				$source_old[$key]['status']    = ($row['status'] == 4) ? 1 : 4;
				$source_old[$key]['next_page'] = '';	
			}
			
			$this->db->update_batch('source_users',$source_old,'id');	
		}
		
		echo response(200, 'update');	
	}
}
?>
<?php (defined('BASEPATH')) OR exit('No direct script access allowed');
class Test extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('curl');
        date_default_timezone_set("America/Los_Angeles");

    }
    public function index($id) {
        $param = array(
            'acc_id'=> $id,
            'proxy'=>true,
            'action_id' => 2
        );

        $header = array(
            'Token' => 'lJCbWloJ1SdvyI9WhLQEx8yHpZtSJiAv'
        );

        $res = $this->curl->instadaily('account/get', true, $param);

        $res = json_decode($res, true);


        $setup = array(
            'followed_last_day' => 5,
            'unfollow_ope' => [2, 4],
            'unfollow_day' => [15, 20],
            'delay_unfollow'=> [15, 20],
            'delay_ope' => [45, 60],
            'execute_time' => [0, 10],
            'unfollow_all' => 1,
            'unfollow_back' => 0
        );

        $setup = json_encode($setup);
        $arr = current($res['data']);
        $arr['setting'] = $setup;
        $arr['results'] = 0;
        $arr['proxy_auth'] = '';

        $url = 'http://unfollow.instadaily.pro/execute/index/32';
        $url = 'http://cronjob-instagram.com/execute/index/'.$id;
        $response = $this->curl->call($url, true, $arr);
        print_r($response);

    }
}
?>
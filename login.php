<?php  (defined('BASEPATH')) OR exit('No direct script access allowed');
class Login extends MY_Controller
{	
	public function __construct(){
   		parent::__construct();
		$this->load->library('curl');
		
   	}
	
	public function instagram()
	{
		$username =  $this->input->post('username');
		$password = $this->input->post('password');
		$proxy_id = $this->input->post('proxy_id');
		
		if( ! $username || ! $password || ! $proxy_id ) {
			echo response('400', 'Có lỗi không xác định!');
			exit;	
		}
		
		
		//check proxy id
		$check_proxy = $this->db->select('id')
		->where(array('user_id'=>$this->user_id,'id'=>$proxy_id))
		->get('proxys')->row_array();
		if(!$check_proxy)

		if(!$proxy_id)

		{
			echo response(400,'Ko ton tai proxy');
			exit;	
		}
		
		$cookie_file = $this->create_file();
		$fp = fopen($cookie_file,'w');
		fclose($fp);
		chmod($cookie_file,0644);
		
		$res = $this->curl->instagram('',false,false,false,$cookie_file,false,true);
		
		
		if($res['code'] != 200) {
			echo response($res['code'], 'Có lỗi không xác định!');
			exit;	
		}
		
		preg_match('/csrftoken=([a-zA-Z0-9]*?);/', $res['response'], $matches);
		
		if( ! $matches ) {
			echo response('400', 'Không tìm thấy token');
			exit;	
		}
		
		$token = $matches[1];
		
		$url = 'accounts/login/ajax/';
		
		$param = array(
			'username' => $username,
			'password' => $password,
			
		);
		
		$param = http_build_query($param);
		
		$header = array(
			'content-type: application/x-www-form-urlencoded',
			'referer: https://www.instagram.com/accounts/login/ajax/',
			'x-csrftoken: '.$token,
			
		);
		
		$res = $this->curl->instagram($url,true,$param,$header,$cookie_file);
		
		$message = json_decode($res['response'],true);

		//Check user ton tai
		$id_insta = isset($message['userId']) ? $message['userId'] : '' ;
		$check = $this->db->where(array('id_insta' =>$id_insta))->get('accounts')->row_array();
		if($check)
		{
			echo response(400,'User da ton tai');
			exit;	
		}
		
		if( isset($message['authenticated']) && !$message['authenticated']) 
		{
			unlink($cookie_file);
			echo response($res['code'], 'Sai tài khoản hoặc mật khẩu');
			exit;
		}
		
		$status         = 1;
		$checkpoint_url = '';
		$message        = 'OK';

		//Nếu checkpoin
		if($res['code'] == 400) {
			$message = 'checkpoint';			
			$status  = 0;
			$checkpoint_url = str_replace('/challenge','challenge',$message['checkpoint_url']);
			$this->sendverifycode($checkpoint_url, $cookie_file, $token);
			
		}

		//Đọc file cookie
		$fp = fopen($cookie_file,'r');
		//Đọc file cookie
		$cookie = fread($fp,filesize($cookie_file));		
		//đóng phiên làm việc của file
		fclose($fp);
		//Xóa link cookie
		unlink($cookie_file);
		//Trả kết quả		
		$new = array(
			'user_id'  => $this->user_id,
			'id_insta' => $id_insta,
			'username' => $username,
			'password' => md5($password),
			'proxy_id' => $proxy_id,

			'cookies'  => $cookie,
			'token'    => $token,
			'checkpoint_url' => $checkpoint_url,
			'status'   => $status //0
		);
	
		$this->db->insert('accounts',$new);
		
		echo response(200,$message);	
	}	
	
	
	public function verify_checkpoint()
	{
		$acc_id = $this->input->post('acc_id');
		$code   = $this->input->post('code');
		
		if(!$acc_id || !$code) {
			response(400,'Error');
			exit;	
		}

		$acc = $this->db->where('id',$acc_id)->get('accounts')->row_array();

		if( !$acc) {
			response(400,'Account khong ton tai');
			exit;	
		}
		
		//create va ghi coookie ra file
		$cookie_file = $this->create_file();
		$fp = fopen($cookie_file,'w');

		fclose($fp);
		chmod($cookie_file,0644);

				
		$param = array(
			'verify_code' => $code
		);
		
		$param = http_build_query($param);
		
				
		$header = array(
			'content-type: application/x-www-form-urlencoded',
			'x-csrftoken: '.$acc['token'],
			'referer: https://www.instagram.com/'.$acc['checkpoint_url'],
			'x-instagram-ajax: 1b36312e9ac7',
			'x-requested-with: XMLHttpRequest'
		);
		
		$res = $this->curl->instagram($acc['checkpoint_url'],true,$param,$header,$cookie_file);
		
		if($res['code'] != 200)
		{
			echo response($res['code'], 'Verify checkpoint error');
			unlink($cookie_file);
			die;			
		}
		
		unlink($cookie_file);
	}
	
	
	private function sendverifycode($url,$cookie_file,$token)
	{
				
		$param = array(
			'choice' => 1
		);
		
		$param = http_build_query($param);
		
				
		$header = array(
			'content-type: application/x-www-form-urlencoded',
			'x-csrftoken: '.$token,
			'referer: https://www.instagram.com/'.$url,
			'x-instagram-ajax: 1b36312e9ac7',
			'x-requested-with: XMLHttpRequest'
		);
		
		$res = $this->curl->instagram($url,true,$param,$header,$cookie_file);
		
		if($res['code'] != 200) 
		{
			echo response($res['code'], 'Send verify code error');
			unlink($cookie_file);
			die;			
		}
	}
	
	
	private function create_file()
	{
		$cookie_file = uniqid().random_string('alnum',8);
		$cookie_file = 'cookies/'.$cookie_file.'.txt';
		
		return $cookie_file;	
	}
	
}?>
